"""
This is an Python TK entry that encrypts each journal entry as a string of text and places it inside of a sqlite database.
We should probably just encrypt the whole sqlite database instead. I'll probably make another implementation that does that.
I also want to make a CLI version too using curses or something like that.

Inspiration for this project was the following:
    - Lifeograph. http://lifeograph.sourceforge.net/wiki/Main_Page
    - Boredome. I needed to make something.

Use lifeograph or some other tool with a richer text editor than this one.


This is really just an example application. Anyone using this or using it as an example proceed at your own peril.

What could be better? Encrypt the entire sqlite database instead of each entry which would eliminate the need for CBC mode,
we could just use EAX or even better RSA(Public Key). If using RSA lets use the biggest bit we can, 4096. Reference:
https://github.com/Legrandin/pycryptodome/blob/50dc173751a5b101a015f5798133d836fe614769/lib/Crypto/PublicKey/RSA.py#L394

# modified example from pycryptodome:
from Crypto.PublicKey import RSA

secret_code = "Unguessable"
key = RSA.generate(4096)
encrypted_key = key.export_key(passphrase=secret_code, pkcs=8,
                              protection="scryptAndAES128-CBC")

file_out = open("rsa_key.bin", "wb")
file_out.write(encrypted_key)
file_out.close()

print(key.publickey().export_key())
"""
from base64 import b64decode
from base64 import b64encode
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import scrypt
from Crypto.Util.Padding import pad, unpad
from Crypto.Random import get_random_bytes
from pathlib import Path
from tkinter import *
from tkinter import ttk
import datetime
import json
import sqlite3
import sys


def encryptText(plainKey, plaintext):
    """
    Encrypt a journal entry using AES 256 CBC

    Uses storeItInSqlite method to put data into Sqlite3 Database
    """
    try:
        # Turn the key into bytes utf-8 encoding.
        key = plainKey.encode("utf-8")
        # Get a salt and derive the key.
        salt = get_random_bytes(16)
        key = scrypt(key, salt, 16, N=2**14, r=8, p=1)
        # Debug key bit size.
        print("key size: ", sys.getsizeof(key), sys.getsizeof(key) * 8)
        cipher = AES.new(key, AES.MODE_CBC)
        # Encrypt plaintext, encoded as utf-8 bytes.
        ct_bytes = cipher.encrypt(pad(plaintext.encode("utf-8"), AES.block_size))
        iv = b64encode(cipher.iv).decode("utf-8")
        ct = b64encode(ct_bytes).decode("utf-8")
        sodium = b64encode(salt).decode("utf-8")
        result = json.dumps({"salt": sodium, "iv": iv, "ciphertext": ct})
        storeItInSqlite(result)
    except (ValueError, KeyError):
        print(ValueError, KeyError)
        print("Incorrect decryption")


def decryptText(plainKey, stuff):
    """
    Decrypt a journal entry selected from the TKInter List Box.
    """
    key = plainKey.encode("utf-8")
    pt = ""
    print(key)
    # Turn the key into bytes utf-8 encoding.
    # Get a salt and derive the key.
    try:
        b64 = json.loads(stuff)
        iv = b64decode(b64["iv"])
        ct = b64decode(b64["ciphertext"])
        salt = b64decode(b64["salt"])
        print("salt", salt)
        key = scrypt(key, salt, 16, N=2**14, r=8, p=1)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        pt = unpad(cipher.decrypt(ct), AES.block_size)
        print("The message was: ", pt)
        return pt
    except (ValueError, KeyError):
        print(ValueError, KeyError)
        print("Incorrect decryption")


def connect(path):
    """
    Connect to the sqlite database.

    This method will create the database if it does not exist yet.

    Excepts SQLite Errors.
    """
    conn = None
    try:
        conn = sqlite3.connect(path)
        print(conn)
        print(sqlite3.version)
        return conn
    except sqlite3.Error as er:
        print("SQLite error: %s" % (" ".join(er.args)))
        print("Exception class is: ", er.__class__)
        print("SQLite traceback: ")
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))


def storeItInSqlite(ciphertext):
    """
    Store the entry in SQlite.

    The entry could be anything to this method. But in context of the application it will be a JSON object.
    """
    # Check if the file exists
    # if exists use it
    path = Path("tkAESJournalStore.db")
    if path.is_file():
        # store stuff
        conn = connect(path)
        cur = conn.cursor()
        insertCommand = """INSERT INTO 'journal' (entrydate, entry) VALUES (?,?);"""
        entryData = (datetime.datetime.now(), ciphertext)
        cur.execute(insertCommand, entryData)
        conn.commit()
        cur.close()
        conn.close()
    else:
        # if not exists create it then use it.
        # Create the file
        conn = connect(path)
        cur = conn.cursor()
        cur.execute(
            "CREATE TABLE journal (id INTEGER PRIMARY KEY AUTOINCREMENT ,entrydate timestamp, entry VARCHAR)"
        )
        conn.commit()
        cur.close()
        conn.close()


def getEntriesFromSqlite():
    """
    Get all the entries from the sqlite database
    """
    path = Path("tkAESJournalStore.db")
    if path.is_file():
        # store stuff
        conn = connect(path)
        cur = conn.cursor()
        selectCommand = """SELECT * FROM journal"""
        result = cur.execute(selectCommand)
        ret = result.fetchall()
        conn.commit()
        cur.close()
        conn.close()
        print(ret)
        return ret


def callback():
    """
    Button call back: Trigger the decryption process.
    """
    plainText = textEditor.get(0.1, END)
    key = keyTextBox.get(0.1, END)
    print(plainText)
    encryptText(key.strip(), plainText)


def onselect(event):
    """
    Select the journal entry to view.
    """
    try:
        # Note here that Tkinter passes an event object to onselect()
        selection = event.widget.curselection()
        index = selection[0]
        value = event.widget.get(index)
        print(value[2])
        journalViewer.delete(1.0, END)
        journalViewer.insert(
            1.0, decryptText(keyTextBox.get(0.1, END).strip(), value[2])
        )
    except:
        print("Encryption Likely Failed. Check the key.")


"""
TKInter Graphics Stuff Below.
"""

root = Tk()

# Main text editor box
editorLabel = Label(root, text="Edit Text Here")
textEditor = Text(root, width=42, height=10)

# list box for journal entries
journalEntriesLabel = Label(root, text="Journal Entries")
journalEntries = Listbox(root, width=42, height=10, name="journalEntries")
entries = getEntriesFromSqlite()
if entries != None:
    for i in range(len(entries)):
        journalEntries.insert(i, entries[i])
journalViewer = Text(root, width=42, height=10)

button1 = Button(root, text="Commit entry?", command=callback)

# The text box for the key
keyTextBoxLabel = Label(root, text="Key to Encrypt the journal entry")
keyTextBox = Text(root, width=42, height=1)

editorLabel.grid(column=0, row=0, sticky=(N, S))
textEditor.grid(column=0, row=1, sticky=(N, S, E, W), padx=10, pady=10)
button1.grid(column=0, row=2, sticky=(N, S, E, W), padx=10, pady=10)
journalEntriesLabel.grid(column=1, row=0, sticky=(N, S, E, W))
journalEntries.grid(column=1, row=1, sticky=(N, S, E, W), padx=10, pady=10)
journalViewer.grid(column=1, row=2, sticky=(N, S, E, W), padx=10, pady=10)

keyTextBoxLabel.grid(column=0, row=3)
keyTextBox.grid(column=0, row=4, sticky=(N, S, E, W), padx=10, pady=10)

root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=1)
root.rowconfigure(0, weight=0)
root.rowconfigure(1, weight=1)
root.rowconfigure(2, weight=1)
root.rowconfigure(3, weight=1)
root.rowconfigure(4, weight=1)

journalEntries.bind("<<ListboxSelect>>", onselect)
root.title("Test")

root.mainloop()
